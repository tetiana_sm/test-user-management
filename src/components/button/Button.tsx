import React, { FC } from 'react';
import clsx from 'clsx';

import { Button } from '@material-ui/core';

import useStyles from './Button.style';

interface ButtonProps {
    children: React.ReactNode;
    onClick?: () => void;
    variant?: 'text' | 'outlined' | 'contained';
    type?: 'submit' | 'button' | 'reset';
    myClass?: any;
}

const ButtonComponent: FC<ButtonProps> = ({ children, variant = 'contained', type = 'button', myClass, onClick }) => {
    const classes = useStyles();

    return (
        <Button
            onClick={onClick}
            variant={variant}
            type={type}
            className={clsx(classes.btn, myClass)}>
            {children}
        </Button>
    )
}

export default ButtonComponent;