import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modal_box: {
        width: 450,
        backgroundColor: theme.palette.background.paper,
        borderRadius: 20,
        padding: '20px 16px'
    },
    modal_title: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 20,
        color: theme.palette.text.primary,
        fontWeight: 'bold',
        textTransform: 'capitalize'
    },
    close_btn: {
        padding: 0,
        color: theme.palette.text.primary
    }
}));

export default useStyles;