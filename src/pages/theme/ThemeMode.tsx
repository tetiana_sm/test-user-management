import React, { FC, useContext, useEffect, useState } from 'react';

import { Switch } from '@material-ui/core';

import useStyles from './ThemeMode.style';
import { ThemeContext } from '../../contexts/themeContext';

const ThemeMode: FC = () => {
    const classes = useStyles();
    const { theme, toggleTheme } = useContext(ThemeContext);
    const [mode, setMode] = useState<boolean>(false);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setMode(event.target.checked);        
    }

    useEffect(() => {
        if (theme === "light") {
            setMode(false)
        } else {
            setMode(true)
        }
    }, [theme])

    useEffect(() => {
        if (mode) {
            localStorage.setItem("theme", "dark");
        } else {            
            localStorage.setItem("theme", "light");
        }
    }, [mode])

    return (
        <Switch
            className={classes.switch}
            checked={mode}
            onChange={handleChange}
            onClick={toggleTheme}
            inputProps={{ 'aria-label': 'controlled' }}
            />
    )
}

export default ThemeMode;