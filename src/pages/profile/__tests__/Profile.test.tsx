import React from 'react';
import { fireEvent, getByText, render } from '@testing-library/react';
import ProfilePage from '../Profile';

const mockDispatch = jest.fn();

jest.mock('react-redux', () => ({
    useSelector: jest.fn(() => [{id: '1', name: 'Tania', phone: 'sm', email: ''}] ),
    useDispatch: () => mockDispatch
}));

jest.mock('react-router-dom', () => ({
    useHistory: jest.fn()
}));

jest.mock('../../../config/firebase', () => ({
    auth: jest.fn().mockReturnThis()
}));

describe('Profile', () => {
    test('useState setState is called', () => {
        const setStateMock = jest.fn();
        const useStateMock: any = (state: any) => [state, setStateMock];
        jest.spyOn(React, 'useState').mockImplementation(useStateMock);

        const { getByText } = render(<ProfilePage />);

        const editBtn = getByText('Edit Profile');
        fireEvent.click(editBtn);

        expect(setStateMock).toHaveBeenCalledWith(true);
    })

    test('', () => {
        const { getByTestId } = render(<ProfilePage />);

        const userName = getByTestId('user-name');

        // console.log(userName)

        expect(userName).toHaveTextContent('Tania')
    })
})