import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    title: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        color: theme.palette.text.primary,
    },
    signout_btn: {
        backgroundColor: theme.palette.text.secondary,
        border: `1px solid ${theme.palette.grey[200]}`,
        color: `${theme.palette.text.primary} !important`,

        '&:hover': {
            backgroundColor: '#dfdfdf'
        }
    },
    avatar: {
        width: theme.spacing(4),
        height: theme.spacing(4),
        marginRight: theme.spacing(1)
    },
    navigation: {
        padding: theme.spacing(0),

        '& li': {
            listStyle: 'none',
            marginRight: theme.spacing(2)
        }
    },
    navigation_title: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        fontWeight: 'bold',
        color: theme.palette.text.primary,
        textDecoration: 'none'
    }
}));

export default useStyles;