import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { act, fireEvent, render } from '@testing-library/react';

import SignInPage from '../SignIn';

const mockStore = configureMockStore([thunk]);
const mockDispatch = jest.fn();

jest.mock('react-redux', () => ({
    useSelector: jest.fn(() => 'Some error message.'),
    useDispatch: () => mockDispatch
}))

jest.mock('../../../config/firebase', () => ({
    auth: jest.fn().mockReturnThis()
}))

describe("signIn", () => {
    test("calls the onSubmit function", async () => {  
        const mockHandleSignIn = jest.fn();
        const { getByLabelText, getByRole } = render(<SignInPage />);
        
        const emailField = getByLabelText('Email');
        const passwordField = getByLabelText('Password');

        await act(async () => {
            fireEvent.change(emailField, { target: {value: 'email@test.com'} });
            fireEvent.change(passwordField, { target: {value: '123123'} });
        })

        await act(async () => {
            fireEvent.click(getByRole('button'));
        })
        
        mockHandleSignIn();
        expect(mockHandleSignIn).toHaveBeenCalled();
    });

    test("with invalid email", async () => {  
        const { getByLabelText, container } = render(<SignInPage />);
        
        await act(async () => {
            const emailField = getByLabelText('Email');
            fireEvent.change(emailField, {target: {value: 'test.mail'}});
            fireEvent.blur(emailField);
        });

        expect(container.innerHTML).toMatch('Enter a valid email.')
    });

    test("with invalid password", async () => {  
        const { getByLabelText, container } = render(<SignInPage />);
        
        await act(async () => {
            const passwordField = getByLabelText('Password');
            fireEvent.change(passwordField, {target: {value: '123'}});
            fireEvent.blur(passwordField);
        });

        expect(container.innerHTML).toMatch('Password must be at least 6 characters.')
        
    });
})