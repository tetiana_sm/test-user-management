import { ThunkAction, ThunkDispatch } from "redux-thunk";
import Firebase, { auth } from "../../config/firebase";
import { query, collection, startAfter, limit, getDocs, startAt, orderBy } from 'firebase/firestore';
import { AuthUser, SignInData, SignUpData, User } from "../../interfaces/data";
import { ADD_USER, GET_USER, REMOVE_USER, SET_ERROR, SET_USER, SIGN_OUT, UPDATE_CURRENT_USER, UPDATE_USER } from "../types";

export const signUp = (data: SignUpData) => {
    return async (dispatch: any) => {
        try {
            const res = await auth.createUserWithEmailAndPassword(data.email, data.password);

            if (res.user) {
                const userState: AuthUser = {
                    name: data.name,
                    email: data.email
                }

                await Firebase.firestore().collection('/accounts').doc(res.user.uid).set({
                    ...userState,
                    id: res.user.uid
                });

                dispatch ({
                    type: SET_USER,
                    payload: {
                        ...userState,
                        id: res.user.uid
                    }
                })
            }
        } catch (error) {
            dispatch ({
                type: SET_ERROR,
                payload: (error as Error).message
            })
        }
    }
}

export const signIn = (data: SignInData) => {
    return async (dispatch: any) => {
        try {
            await auth.signInWithEmailAndPassword(data.email, data.password);
        } catch (error) {
            dispatch ({
                type: SET_ERROR,
                payload: (error as Error).message
            })
        }
    }
}

export const signOut = () => {
    return async (dispatch: any) => {
        try {
            await auth.signOut();

            dispatch ({
                type: SIGN_OUT
            })
        } catch (error) {
            console.log('error', error)
        }
    }
}

export const updateCurrentUser = (user: AuthUser) => {
    return async (dispatch: any) => {
        try {           
            await Firebase.firestore().collection('accounts').doc(user.id).update({
                id: user.id,
                name: user.name,
                phone: user.phone,
                email: user.email,
                avatar: {
                    name: user.file?.name,
                    type: user.file?.type,
                    url: user.file.url,
                } 
            });

            dispatch ({
                type: UPDATE_CURRENT_USER,
                payload: {
                    ...user,
                    avatar: {
                        name: user.file?.name,
                        type: user.file?.type,
                        url: user.file.url,
                    } 
                }
            })
        } catch (error) {
            console.log('error', error)
        }
    }
}

export const getUserById = (id: string) => {
    return async (dispatch: any) => {
        try {
            const authUser = await Firebase.firestore().collection('accounts').doc(id).get();

            if (authUser.exists) {
                const userData = authUser.data() as AuthUser;

                dispatch ({
                    type: SET_USER,
                    payload: userData
                })
            }
        } catch (error) {
            dispatch ({
                type: SET_ERROR,
                payload: (error as Error).message
            })
        }
    }
}

export const setError = (message: string) => {
    return (dispatch: any) => {
        dispatch ({
            type: SET_ERROR,
            payload: message
        })
    }
}

export const getUsers = (loader: boolean, lastVisible?: string) => {
    return async (dispatch: any) => {
        try {
            let docs: any;

            if (loader) {
                docs = query(collection(Firebase.firestore(), "users"), orderBy('id'), startAfter(lastVisible), limit(1));
            } else {
                docs = query(collection(Firebase.firestore(), "users"), limit(1));
            }

            const documentSnapshots = await getDocs(docs);

            const users = documentSnapshots.docs.map((doc: any) => ({
                id: doc.id,
                ...doc.data()
            }))

            dispatch ({
                type: GET_USER,
                payload: users
            })
        } catch (error) {
            dispatch ({
                type: SET_ERROR,
                payload: (error as Error).message
            })
        }
    }
}

export const addUser = (user: User) => {
    return async (dispatch: any) => {
        try {
            // const storageRef = Firebase.storage().ref();
            // const fileRef = storageRef.child(`images/${user?.image?.name}`);

            // await fileRef.put(user?.image);

            // const newUser = {                
            //     firstName: user.firstName,
            //     lastName: user.lastName,
            //     email: user.email,
            //     image: {
            //         name: user.image.name,
            //         type: user.image.type,
            //         url: await fileRef.getDownloadURL()
            //     } 
            // }

            Firebase.firestore().collection('/users').doc(user.id).set(user);

            dispatch ({
                type: ADD_USER,
                payload: user
            })
        } catch (error) {
            dispatch ({
                type: SET_ERROR,
                payload: (error as Error).message
            })
        }
    }
}

export const updateUser = (user: User) => {
    return async (dispatch: any) => {
        try {
            let updatedUser: User = {  
                id: user.id,              
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email
            }

            if (user.image) {
                const storageRef = Firebase.storage().ref();
                const fileRef = storageRef.child(`images/${user?.image?.name}`);
    
                await fileRef?.put(user?.image);

                updatedUser = {
                    ...updatedUser,
                    image: {
                        name: user.image.name,
                        type: user.image.type,
                        url: await fileRef.getDownloadURL()
                    } 
                }
            }

            Firebase.firestore().collection('/users').doc(user.id).update(updatedUser);

            dispatch ({
                type: UPDATE_USER,
                payload: updatedUser
            })
        } catch (error) {
            dispatch ({
                type: SET_ERROR,
                payload: (error as Error).message
            })
        }
    }
}

export const removeUser = (id: string) => {
    return async (dispatch: any) => {
        try {
            await Firebase.firestore().collection('users').doc(id).delete();

            dispatch ({
                type: REMOVE_USER,
                payload: id
            })
        } catch (error) {
            dispatch ({
                type: SET_ERROR,
                payload: (error as Error).message
            })
        }
    }
}