import React, { FC, forwardRef } from 'react';
import { TextField } from '@material-ui/core';

import useStyles from './Input.style';

interface PropsType {
    name: string,
    type: string,
    label: string,
    placeholder?: string,
    error?: boolean,
    helperText?: string
}

export const Input = forwardRef((props: PropsType, ref) => {
    const classes = useStyles();

    return (
        <TextField
            fullWidth
            className={classes.input}
            variant="outlined"
            autoComplete="off"
            inputRef={ref}
            {...props} />
    )
});