import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    '@global': {
        '.empty .MuiOutlinedInput-notchedOutline': {
            borderColor: '#f00'
        }
    },
    container: {
        width: 'calc(100% + 16px)',
        height: '100vh',
        margin: theme.spacing(-1),
        marginTop: -46,
        paddingTop: 38,
        background: theme.palette.background.paper,

        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    signUpForm: {
        width: 300,
        backgroundColor: theme.palette.background.default,
        borderRadius: 20,
        padding: theme.spacing(2)
    },
    title: {        
        fontFamily: '"Roboto", sans-serif',
        fontSize: 20,
        fontWeight: 600,
        color: theme.palette.text.primary,
        marginBottom: theme.spacing(2.5)
    },
    signUpBtn: {
        backgroundColor: '#888fdc',
        boxShadow: 'none',
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        color: '#FFF',
        textTransform: 'capitalize',
        
        '&:hover': {
            boxShadow: 'none',
            backgroundColor: '#888fdc'
        }
    },
    text: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 14,
        color: theme.palette.text.primary
    },
    link: {
        color: '#888fdc',
        paddingLeft: theme.spacing(1)
    },
    progress: {
        height: 3,
        backgroundColor: '#ccc',
        position: 'relative',
        borderRadius: 4,
        marginBottom: theme.spacing(2.5)
    },
    progress__color: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        borderRadius: 5
    }
}));

export default useStyles;