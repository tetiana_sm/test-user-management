import React, { FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import clsx from 'clsx';

import { Box, Button, Container, Typography } from '@material-ui/core';

import AlertComponent from '../../components/alert/Alert';
import { Input } from '../../components/input/Input';

import useStyles from './SignIn.style';

import { RootState } from '../../store';
import { signIn } from '../../store/actions/auth';
import { SignInData } from '../../interfaces/data';

const schema = yup.object().shape({
    email: yup.string().email('Enter a valid email.').required('This is a required field.'),
    password: yup.string().min(6, 'Password must be at least 6 characters.').required()
});

const SignInPage: FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector((state: RootState) => state.auth.error);
    const { register, handleSubmit, formState: { errors } } = useForm<SignInData>({
        resolver: yupResolver(schema),
        mode: "onBlur"
    });

    const handleSignIn = handleSubmit((data: SignInData) => {
        dispatch(signIn(data))
    })

    return (
        <Container className={classes.container}>
            <Box display="flex" flexDirection="column" className={classes.signInForm}>
                <Typography component="h3" className={classes.title} align="center">Sign In</Typography>

                {
                    error && <AlertComponent severity="error" text={error} />
                }
                
                <Input
                    {...register('email')}
                    name="email"
                    type="text"
                    label="Email"
                    error={!!errors.email}
                    helperText={errors?.email?.message} />
                
                <Input
                    {...register('password')}
                    name="password"
                    type="password"
                    label="Password"
                    error={!!errors.password}
                    helperText={errors?.password?.message} />

                <Button fullWidth variant="contained" onClick={handleSignIn} className={clsx(classes.signInBtn, "signIn-btn")}>Sign In</Button>
            </Box>
        </Container>
    )
};

export default SignInPage;