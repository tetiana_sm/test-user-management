const firebase = {
  auth: jest.fn().mockReturnThis(),
  createUserWithEmailAndPassword: jest.fn((email: string, password: string) => Promise.resolve(true)),
  signInWithEmailAndPassword: jest.fn((email: string, password: string) => Promise.resolve()),
  signOut: jest.fn().mockReturnThis(),
  firestore: jest.fn().mockReturnValue({
      collection: jest.fn().mockReturnValue({
        get: jest.fn(() => Promise.resolve([
          { id: '1', firstName: 'Bob', lastName: 'builder', email: 'bob@mail.com' }        
        ])),
        doc: jest.fn().mockReturnValue({
          set: jest.fn(() => Promise.resolve({
             id: '2', firstName: 'Test', lastName: 'name', email: 'test@mail.com'
          })),
          update: jest.fn(() => Promise.resolve({ firstName: 'Update first name' })),
          delete: jest.fn().mockReturnThis()
        })
      })
    })
}

export default firebase;