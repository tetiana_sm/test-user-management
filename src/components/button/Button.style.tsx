import { makeStyles } from "@material-ui/core";
import { ThemeContext } from "../../contexts/themeContext";

const useStyles = makeStyles((theme) => ({
    btn: {
        boxShadow: 'none',
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        color: theme.palette.common.white,
        textTransform: 'capitalize',
        
        '&:hover': {
            boxShadow: 'none'
        }
    }
}));

export default useStyles;