import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    switch: {
        marginLeft: theme.spacing(1.875),

        '& .MuiSwitch-track': {
            backgroundColor: theme.palette.text.primary
        }
    }
}));

export default useStyles;