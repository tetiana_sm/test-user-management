import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    container: {
        width: 'calc(100% + 16px)',
        height: '100vh',
        margin: theme.spacing(-1),
        marginTop: -59,
        paddingTop: 51,
        background: theme.palette.background.paper
    },
    avatar: {
        width: 76,
        height: 76
    },
    user_title: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        color: '#ccc'
    },
    user_subtitle: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        fontWeight: 600,
        color: theme.palette.text.primary
    },
    user_textarea: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        color: theme.palette.text.primary,
        
        '& .MuiInput-underline::before': {
            borderColor: 'rgba(136, 143, 220, .2)',
            borderWidth: 2
        },
        
        '& .MuiInput-underline::after': {
            borderColor: theme.palette.primary.main
        }
    },
    edit_btn: {
        backgroundColor: theme.palette.primary.main,
        color: '#fff',
        marginTop: theme.spacing(2)
    },
    cancel_btn: {
        backgroundColor: '#dc8888',
        color: '#fff'
    },
    save_btn: {
        backgroundColor: '#88dcb1',
        color: '#fff',
        marginLeft: theme.spacing(2)
    }
}));

export default useStyles;