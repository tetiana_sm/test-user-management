import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { fireEvent, render } from '@testing-library/react';

import ManageUsers from '../ManageUsers';

let mockStore = configureMockStore([thunk]);
const mockDispatch = jest.fn();

jest.mock('react-redux', () => ({
    useSelector: jest.fn(() => [{id: '1', firstName: 'Test', lastNmae: '1', email: ''}] ),
    useDispatch: () => mockDispatch
}));

jest.mock('react-router-dom', () => ({
    useHistory: jest.fn()
}));

jest.mock('../../../config/firebase', () => ({
    auth: jest.fn().mockReturnThis()
}));

describe('Search input value', () => {  
    test('update on change', () => {
        const { getByPlaceholderText  } = render(<ManageUsers />);

        const searchInput = getByPlaceholderText ('Search...');

        fireEvent.change(searchInput, {target: {value: 'Test'}});

        expect(searchInput).toHaveValue('Test')
    })
})