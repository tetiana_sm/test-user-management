import { useState } from 'react';

export const useAvatarEditor = (file: any, editor: any) => {
    const [avatar, setAvatar] = useState<any>(null);

    const handleSaveAvatar = () => {
        if (editor) {
            const canvasScaled = editor.getImageScaledToCanvas();
            const croppedImg = canvasScaled.toDataURL();
    
            setAvatar({            
                name: file.name,
                type: file.type,
                url: croppedImg
            })
        }
    }
    
    return [avatar, handleSaveAvatar];
}