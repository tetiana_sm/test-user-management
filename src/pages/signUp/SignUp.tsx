import React, { FC, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import clsx from 'clsx';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

import { Box, Button, Container, Typography } from '@material-ui/core';

import { Input } from '../../components/input/Input';
import AlertComponent from '../../components/alert/Alert';

import useStyles from './SignUp.style';

import { RootState } from '../../store';
import { signUp } from '../../store/actions/auth';
import { SignUpData } from '../../interfaces/data';

let schema = yup.object().shape({
    name: yup.string().notRequired(),
    email: yup.string().email().required('This is a required field.'),
    password: yup
        .string()
        .min(6)
        .required("Please provide a valid password")
        .matches(/(?=.*[a-z])/, 'Lowercase')
        .matches(/(?=.*[A-Z])/, 'Uppercase')
        .matches(/(?=.*[0-9])/, 'Number')
        .matches(/(?=.*[!@#\$%\^&\*])/, 'Special character')    
})

const SignUp: FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector((state: RootState) => state.auth.error);
    const { register, handleSubmit, formState: { errors } } = useForm<SignUpData>({        
        criteriaMode: 'all',
        reValidateMode: 'onChange',
        mode: 'onChange',
        resolver: yupResolver(schema, { abortEarly: false })
    });

    const [width, setWidth] = useState<number>();
    const [bgColor, setBgColor] = useState<string>('');

    const handleSignUp = handleSubmit((data: SignUpData) => {
        dispatch(signUp(data));
    })

    useEffect(() => {
        errors.password && validateStrengthPass()
    }, [errors.password])

    function validateStrengthPass () {
        if (typeof errors.password?.types?.matches === 'object' && errors.password?.types?.matches?.length > 1) {
            setWidth(20);
            setBgColor('#ff4757');
        } else if (typeof errors.password?.types?.matches === 'string') {
            setWidth(50);
            setBgColor('#ffa500');
        } else if (!errors.password?.types?.hasOwnProperty('matches')) {
            setWidth(100);
            setBgColor('#4dbc7a');        
        }
    }

    return (
        <Container className={classes.container}>
            <Box display="flex" flexDirection="column" className={classes.signUpForm}>
                <Typography component="h3" align="center" className={classes.title} data-testid="title">Sign Up</Typography>

                {
                    error && <AlertComponent severity="error" text={error} />
                }
                
                <Input
                    {...register('name')}
                    name="name"
                    type="text"
                    label="User name"
                    placeholder="User name"
                    error={!!errors.name}
                    helperText={errors?.name?.message} />
                
                <Input
                    {...register('email')}
                    name="email"
                    type="text"
                    label="Email"
                    error={!!errors.email}
                    helperText={errors?.email?.message} />
            
                <Input
                    {...register('password')}
                    name="password"
                    type="password"
                    label="Password"
                    error={!!errors.password}
                    helperText={errors?.password?.message} />

                {
                    errors.password && <Box className={classes.progress}>
                        <Box className={classes.progress__color} sx={{ width: `${width}%`, bgcolor: bgColor }}></Box>
                    </Box> 
                }                

                <Button
                    fullWidth
                    variant="contained"
                    type="submit"
                    onClick={handleSignUp}
                    className={classes.signUpBtn}>
                        Sign up
                </Button>
            
                <Box mt={2} display="flex" justifyContent="center">
                    <Typography component="span" className={classes.text}>Already have an account?</Typography>
                    {/* <Link to="/login" className={clsx(classes.link, classes.text)}>Sign in</Link> */}
                </Box>
            </Box>
        </Container>
    )
};

export default SignUp;