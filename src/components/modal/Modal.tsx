import React, { FC } from 'react';

import { Close } from '@material-ui/icons';
import { Box, IconButton, Modal, Typography } from '@material-ui/core';

import useStyles from './Modal.style';

interface ModalProps {
    open: boolean;
    title: string;
    handleClose: Function;
    children: React.ReactNode;
}

const ModalComponent: FC<ModalProps> = ({ open, title, handleClose, children }) => {
    const classes = useStyles();

    return (
        <Modal
            open={open}
            className={classes.modal}
            aria-labelledby="modal-with-user">
            <Box className={classes.modal_box}>
                <Box display="flex" alignItems="center" justifyContent="space-between" mb={3}>
                    <Typography component="h5" align="center" className={classes.modal_title}>{title}</Typography>
                    <IconButton className={classes.close_btn} aria-label="close modal" size="medium" onClick={() => handleClose()}><Close /></IconButton>
                </Box>

                {children}
                
            </Box>
        </Modal>
    )
};

export default ModalComponent;