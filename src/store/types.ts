export const SET_USER = 'SET_USER';
export const SET_ERROR = 'SET_ERROR';
export const SIGN_OUT = 'SIGN_OUT';
export const UPDATE_CURRENT_USER = 'UPDATE_CURRENT_USER';

export const GET_USER = 'GET_USER';
export const ADD_USER = 'ADD_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const REMOVE_USER = 'REMOVE_USER';