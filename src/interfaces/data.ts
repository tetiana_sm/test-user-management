export interface AuthUser {
    id?: string;
    name?: string;
    email: string;
    phone?: string;
    file?: any;
    position?: any;
}

export interface User {
    id?: string | undefined;
    firstName: string;
    lastName: string;
    email: string;
    image?: any;
}

export interface AuthState {
    user: AuthUser | null;
    users: Array<User>;
    error: string;
}

export interface SignUpData {
    name?: string;
    email: string;
    password: string;
}

export interface SignInData {
    email: string;
    password: string;
}