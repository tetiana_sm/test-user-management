import React, { FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AvatarEditor from 'react-avatar-editor';
import clsx from 'clsx';

import { Avatar, Box, Container, TextField, Typography } from '@material-ui/core';

import useStyles from './Profile.style';

import ButtonComponent from '../../components/button/Button';
import ImageResize from '../../components/image/ImageResize';

import { RootState } from '../../store';
import { updateCurrentUser } from '../../store/actions/auth';


const ProfilePage: FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector((state: RootState) => state.auth.user);
    const [edit, setEdit] = useState<boolean>(false);
    const [name, setName] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [phone, setPhone] = useState<string>('');

    const [editor, setEditor] = useState<AvatarEditor>();
    const [file, selectFile] = useState<any>(null);

    const handleEditProfile = () => {
        setEdit(!edit);
        setName(user.name);
        setEmail(user.email);
        setPhone(user.phone);
    }

    const handleSaveEditProfile = () => {
        if (editor) {
            const canvasScaled = editor.getImageScaledToCanvas();
            const croppedImg = canvasScaled.toDataURL();
            const updatedUser = {
                name,
                email,
                phone,
                id: user.id,
                file: {
                    name: file.name,
                    type: file.type,
                    url: croppedImg
                }
            };

            dispatch(updateCurrentUser(updatedUser));
            setEdit(!edit);
        }
    }

    return (
        <Container className={classes.container}>
            <Box mt={4}>
                <Box
                    display="flex"
                    flexDirection="row"
                    alignItems="flex-end"
                    justifyContent="space-between">                    
                    { edit
                        ? <ImageResize
                            file={file}
                            selectFile={selectFile}
                            setEditor={setEditor} />
                        : <Avatar
                            className={classes.avatar}
                            alt="Avatar Picture"
                            src={user && user.avatar?.url} />
                    }
                </Box>

                <Box>
                    <Box display="flex" flexDirection="column" mt={2}>
                        <Typography component="h4" className={classes.user_title}>Name</Typography>
                        {
                            edit
                            ? <TextField className={classes.user_textarea} name="name" type="text" value={name} placeholder="Enter your name" onChange={event => setName(event.target.value)} /> 
                            : <Typography component="h6" data-testid="user-name" className={classes.user_subtitle}>{user && user.name}</Typography>
                        }                    
                    </Box>

                    <Box display="flex" flexDirection="column" mt={2}>
                        <Typography component="h4" className={classes.user_title}>Email</Typography>
                        {
                            edit
                            ? <TextField className={classes.user_textarea} name="email" type="text" value={email} placeholder="Enter your email" onChange={event => setEmail(event.target.value)} /> 
                            : <Typography component="h6" className={classes.user_subtitle}>{user && user.email}</Typography>
                        }
                    </Box>

                    <Box display="flex" flexDirection="column" mt={2}>
                        <Typography component="h4" className={classes.user_title}>Phone</Typography>
                        {
                            edit
                            ? <TextField className={classes.user_textarea} name="phone" type="text" value={phone} placeholder="Enter your phone" onChange={event => setPhone(event.target.value)} /> 
                            : <Typography component="h6" className={classes.user_subtitle}>{user && user.phone}</Typography>
                        }
                    </Box>
                </Box>
            </Box>

            { edit
                ? <Box className="buttons-group" mt={2}>
                    <ButtonComponent myClass={classes.cancel_btn} onClick={() => setEdit(!edit)}>Cancel</ButtonComponent>
                    <ButtonComponent myClass={classes.save_btn} onClick={handleSaveEditProfile}>Save</ButtonComponent>
                </Box>
                : <ButtonComponent onClick={handleEditProfile} myClass={classes.edit_btn}>Edit Profile</ButtonComponent>
            }
        </Container>
    )
};

export default ProfilePage;