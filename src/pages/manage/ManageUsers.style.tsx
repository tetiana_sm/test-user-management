import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
console.log(theme)
return ({
    container: {
        width: 'calc(100% + 16px)',
        height: '100vh',
        margin: theme.spacing(-1),
        marginTop: -59,
        paddingTop: 51,
        background: theme.palette.background.paper
    },
    add_btn: {
        backgroundColor: theme.palette.primary.main,
        
        '&:hover': {
            backgroundColor: theme.palette.primary.main
        }
    },
    table_head: {
        backgroundColor: theme.palette.background.default,

        '& th': {
            fontFamily: '"Roboto", sans-serif',
            fontSize: 16,
            color: theme.palette.text.primary,
            fontWeight: 'bold',
            textTransform: 'capitalize',
            borderColor: theme.palette.text.primary
        }
    },
    table_cell: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 14,
        color: theme.palette.text.primary,
        borderColor: theme.palette.text.primary
    },
    loading_more_btn: {
        backgroundColor: theme.palette.primary.main,
        color: '#fff',
        marginTop: theme.spacing(2)
    },
    table_btn: {
        color: theme.palette.text.primary
    },
    modal_btn: {
        width: '100%',
        backgroundColor: theme.palette.primary.main
    },
    search_field: {
        '& .MuiInputBase-input': {
            padding: theme.spacing(1.5, 2),
            fontFamily: '"Roboto", sans-serif',
            fontSize: 14,
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.background.default
        },

        '& .MuiOutlinedInput-notchedOutline': {
            borderColor: theme.palette.grey[400]
        }
    }
})})

export default useStyles;