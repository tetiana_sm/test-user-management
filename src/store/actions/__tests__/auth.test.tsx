import React from 'react';
import thunk from 'redux-thunk';
import createMockStore from 'redux-mock-store';
import { GET_USER, SET_ERROR, SET_USER, SIGN_OUT } from '../../types';
import { getUsers, removeUser, setError, signIn, signOut, signUp } from '../auth';
import firebaseMock from '../../../__mocks__/firebase-mock';

const middlewares = [thunk];
const mockStore = createMockStore(middlewares);

jest.mock('../../../config/firebase', () => ({
    auth: jest.fn().mockReturnThis()
}))

describe('auth', () => {
    let store: any;
  
    beforeEach(() => {
        store = mockStore({
            user: null,
            users: [],
            error: ''
        });
    });

    afterEach(() => {
        jest.clearAllMocks();
    })

    describe('#auth', () => {
        test('registration new user', async () => {
            await firebaseMock.createUserWithEmailAndPassword('auth@.mail.co', 'auth123')

            expect(true).toBe(true)
        });

        test('should signIn success', async () => {
            const mockedUser = { email: 'test@test.com', password: '111111' };

            await firebaseMock.auth().signInWithEmailAndPassword(mockedUser.email, mockedUser.password);
            expect(firebaseMock.auth().signInWithEmailAndPassword).toBeCalledWith(mockedUser.email, mockedUser.password)
        });

        test('should signOut user', async () => {
            await firebaseMock.auth().signOut();
        });

        test('return error message', () => {
            const errorMessage = 'Error message.';

            const expectedAction = [
                {
                    type: SET_ERROR,
                    payload: errorMessage
                }
            ]

            store.dispatch(setError(errorMessage))

            expect(store.getActions()).toEqual(expectedAction)
        });

        test('return getting users', () => {
            const users = firebaseMock.firestore().collection('users').get();

            expect(firebaseMock.firestore().collection).toBeCalledWith('users');
            users.then((res: any) => {
                expect(res).toStrictEqual([
                    { id: '1', firstName: 'Bob', lastName: 'builder', email: 'bob@mail.com' }
                ]);
            })
            
            // expect(store.getActions()).toEqual(expectedAction);
        });

        test('set new user', () => {
            const data = firebaseMock.firestore().collection('/testCollection').doc('3').set();

            expect(firebaseMock.firestore().collection).toBeCalledWith('/testCollection');
            expect(firebaseMock.firestore().collection().doc).toBeCalledWith('3');
            data.then((res: any) => {
                expect(res.firstName).toBe('Test');
                expect(res.lastName).toBe('name');
                expect(res.email).toBe('test@mail.com');
            })
        });

        test('update user by id', () => {
            const data = firebaseMock.firestore().collection('testCollection').doc('2').update();
            
            data.then((val: any) => {
                expect(val.firstName).toBe('Update first name')
            })
        });

        test('delete user by id', async () => {
            try {
                await firebaseMock.firestore().collection('testCollection').doc('2').delete();

                expect(true).toBe(false)
            } catch (err) {
                expect(true).toBe(true);
            }
        })
    });
});