import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    container: {
        width: 'calc(100% + 16px)',
        height: '100vh',
        margin: theme.spacing(-1),
        marginTop: -46,
        paddingTop: 38,
        background: theme.palette.background.paper,
        
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    signInForm: {
        width: 300,
        backgroundColor: theme.palette.background.default,
        borderRadius: 20,
        padding: theme.spacing(2)
    },
    title: {        
        fontFamily: '"Roboto", sans-serif',
        fontSize: 20,
        fontWeight: 600,
        color: theme.palette.text.primary,
        marginBottom: theme.spacing(2.5)
    },
    signInBtn: {
        backgroundColor: '#888fdc',
        boxShadow: 'none',
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        color: '#FFF',
        textTransform: 'capitalize',
        
        '&:hover': {
            boxShadow: 'none',
            backgroundColor: '#888fdc'
        }
    }
}));

export default useStyles;