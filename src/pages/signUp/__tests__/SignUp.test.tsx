import React from 'react';
import { act, fireEvent, render } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import SignUp from '../SignUp';

const mockStore = configureMockStore([thunk]);
const mockDispatch = jest.fn();

jest.mock('react-redux', () => ({
    useSelector: jest.fn(() => 'Some error message.'),
    useDispatch: () => mockDispatch
}))

jest.mock('../../../config/firebase', () => ({
    auth: jest.fn().mockReturnThis()
}))

describe('#SignUp', () => {
    test('has correct title text', () => {
        const { container } = render(<SignUp />)
        const title = container.querySelector('h3');
        
        expect(title).toHaveTextContent('Sign Up');
    });

    test('with valid inputs', async () => {
        const { getByLabelText } = render(<SignUp />);

        const emailField = getByLabelText('Email');

        await act(async () => {
            fireEvent.change(emailField, {target: {value: 'email@test.com'}})
        })

        expect(emailField).toBeInTheDocument();
        expect(emailField).toHaveValue('email@test.com');
    });

    test('calls the onSubmit button', async () => {
        const mock = jest.fn();
        const { getByLabelText, getByRole } = render(<SignUp />);

        const nameField = getByLabelText('User name');
        const emailField = getByLabelText('Email');
        const passwordField = getByLabelText('Password');

        await act(async () => {
            fireEvent.change(nameField, { target: {value: 'Test'} });
            fireEvent.change(emailField, { target: {value: 'email@test.com'} });
            fireEvent.change(passwordField, { target: {value: '123123'} });
        })

        await act(async () => {
            fireEvent.click(getByRole('button'));
        })
        
        mock();
        expect(mock).toHaveBeenCalled();
    })
})

