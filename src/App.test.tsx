import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

describe("render component", () => {
  test("renders App component", () => {    
    const { getByText } = render(<App />);
    const text = getByText('123');
    expect(text).toBeInTheDocument();
  })
})
