import React, { ChangeEvent, FC, useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

import { Edit, Delete } from '@material-ui/icons';
import { Box, Container, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, IconButton, Button, TextField } from '@material-ui/core';

import useStyles from './ManageUsers.style';

import ButtonComponent from '../../components/button/Button';
import { Input } from '../../components/input/Input';

import ModalComponent from '../../components/modal/Modal';

import { addUser, getUsers, removeUser } from '../../store/actions/auth';
import { RootState } from '../../store';
import { User } from '../../interfaces/data';

let schema = yup.object().shape({
    firstName: yup.string().notRequired(),   
    lastName: yup.string().notRequired(),   
    email: yup.string().email().required('This is a required field.')
})

const ManageUsers: FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const users = useSelector((state: RootState) => state.auth.users);
    const [open, setOpen] = useState<boolean>(false);
    const [edit, setEdit] = useState<boolean>(false);
    const [updateState, setUpdateState] = useState(null);
    const [search, setSearch] = useState<string>('');
    const [loadingMore, setLoadingMore] = useState<boolean>(false);

    const [fetchUsers, setFetchUsers] = useState<Array<any>>([]);
    const [lastVisible, setLastVisible] = useState(null);

    const { register, handleSubmit, formState: { errors }, reset } = useForm<User>({
        resolver: yupResolver(schema, { abortEarly: false })
    })

    useEffect(() => {
        dispatch(getUsers(loadingMore));
    }, [])

    useEffect(() => {
        if (loadingMore) {
            const lastVisibleId = fetchUsers[fetchUsers.length - 1].id;
            setLastVisible(lastVisibleId);

            if (lastVisible !== lastVisibleId) {
                dispatch(getUsers(loadingMore, lastVisibleId));
                setLoadingMore(false);
            }
        }
    }, [loadingMore])

    useEffect(() => {
            setFetchUsers([...fetchUsers, ...users]);
    }, [users])

    const handleRemoveUser = (id: string) => {
        dispatch(removeUser(id))
    }

    const handleSearchUser = (event: ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value);
    }

    const findUsers = fetchUsers.filter((user: User) => {
        if (!search.length) {
            return users;
        } else if (search.toLowerCase().includes(user.firstName.toLowerCase()) || search.toLowerCase().includes(user.lastName.toLowerCase()) || search.toLowerCase().includes(user.email.toLowerCase())) {
            return users;
        }
    })

    const handleUser = handleSubmit((data: User) => {
        if (edit) {
            console.log(data);
        } else {
            const newUser = {
                id: new Date().getTime().toString(),
                ...data
            };
    
            dispatch(addUser(newUser));
            // setOpen(!open);
            reset();
        }
    })

    return (
    <Container className={classes.container}>
        <Box display="flex" justifyContent="space-between" alignItems="center" mt={4} mb={2}>
            <Box>
                <TextField
                    className={classes.search_field}
                    name="search"
                    type="text"
                    variant="outlined"
                    placeholder="Search..."
                    onChange={handleSearchUser} />
            </Box>

            <ButtonComponent variant="contained" onClick={() => setOpen(true)} myClass={classes.add_btn}>
                Add User
            </ButtonComponent>
        </Box>

        <TableContainer>
            <Table>
                <TableHead className={classes.table_head}>
                    <TableRow>
                        <TableCell>First name</TableCell>
                        <TableCell>Last name</TableCell>
                        <TableCell>E-mail</TableCell>
                        <TableCell>Profile picture</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        findUsers ? findUsers.map((user: any) => (
                            <TableRow key={user.id}>
                                <TableCell className={classes.table_cell}>{user.firstName}</TableCell>
                                <TableCell className={classes.table_cell}>{user.lastName}</TableCell>
                                <TableCell className={classes.table_cell}>{user.email}</TableCell>
                                <TableCell className={classes.table_cell}>{user.image && <img src={user.image.url} alt="User Image" width="100" height="auto" />}</TableCell>
                                <TableCell align="right" className={classes.table_cell}>
                                    <IconButton className={classes.table_btn} aria-label="edit" size="small" onClick={() => {setOpen(true); setEdit(true); handleUser(user)}}><Edit /></IconButton>
                                    <IconButton className={classes.table_btn} aria-label="delete" size="small" onClick={() => handleRemoveUser(user.id)}><Delete /></IconButton>
                                </TableCell>
                            </TableRow>
                        )) : (
                            <TableRow>
                                <TableCell colSpan={6} className={classes.table_cell}>
                                    There are no users yet.
                                </TableCell>
                            </TableRow>
                        )
                    }
                </TableBody>
            </Table>
        </TableContainer>

        <ButtonComponent myClass={classes.loading_more_btn} onClick={() => setLoadingMore(true)}>
            Loading More
        </ButtonComponent>

        <ModalComponent
            open={open}
            title={edit ? 'Edit user' : 'Add user'}
            handleClose={() => { setOpen(!open); setEdit(!edit) }}>
                <form onSubmit={handleUser}>
                    <Input
                        {...register('firstName')}
                        name="firstName"
                        type="text"
                        label="First Name"
                        error={!!errors.firstName}
                        helperText={errors?.firstName?.message} />
                    
                    <Input
                        {...register('lastName')}
                        name="lastName"
                        type="text"
                        label="Last Name"
                        error={!!errors.lastName}
                        helperText={errors?.lastName?.message} />
                    
                    <Input
                        {...register('email')}
                        name="email"
                        type="text"
                        label="Email"
                        error={!!errors.email}
                        helperText={errors?.email?.message} />

                    <ButtonComponent type="submit" myClass={classes.modal_btn}>
                        {edit ? 'Edit User' : 'Add User'}
                    </ButtonComponent>
                </form>
        </ModalComponent>
    </Container>
    )
};

export default ManageUsers;