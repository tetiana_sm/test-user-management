import React, { FC, useContext } from 'react';
import { Avatar, Box, Typography } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import useStyles from './Header.style';

import ButtonComponent from '../button/Button';
import ThemeMode from '../../pages/theme/ThemeMode';

import { signOut } from '../../store/actions/auth';
import { RootState } from '../../store';

const Header: FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const authUser = useSelector((state: RootState) => state.auth.user);

    const handleSignOut = () => {
        dispatch(signOut())
    }

    return (
        <Box display="flex" alignItems="center" flexDirection="row" justifyContent="space-between">
            { authUser
                ? <>
                    <ul style={{ display: 'flex', flexDirection: 'row' }} className={classes.navigation}>
                        <li>
                            <Link to="/profile" className={classes.navigation_title}>Profile</Link>
                        </li>
                        <li>
                            <Link to="/manageusers" className={classes.navigation_title}>Manage</Link>
                        </li>
                    </ul>
                
                    <Box display="flex" flexDirection="row">
                        <Box display="flex" flexDirection="row" alignItems="center" mr={3}>
                            <Avatar src={authUser?.avatar?.url} alt="user avatar" className={classes.avatar} />
                            <Typography component="span" className={classes.title}>
                                {authUser && authUser.name}
                            </Typography>
                        </Box>

                        <ButtonComponent
                            variant="contained"
                            onClick={handleSignOut}
                            myClass={classes.signout_btn}>
                            Signout
                        </ButtonComponent>
                        
                        <ThemeMode />
                    </Box>
                </>
                : <Box display="flex" width={1} justifyContent="flex-end">
                    <ThemeMode />
                  </Box>
            }
        </Box>
    )
}

export default Header;