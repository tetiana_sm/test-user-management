import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage';

const config = {
    apiKey: "AIzaSyBgnLUZmWMyhJzal77eI8IaUKAOQ3cc9wo",
    authDomain: "auth-f26dd.firebaseapp.com",
    projectId: "auth-f26dd",
    storageBucket: "auth-f26dd.appspot.com",
    messagingSenderId: "484496990745",
    appId: "1:484496990745:web:9ccbf80493376a11ef0088"
}

// apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
// authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
// projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
// storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
// messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
// appId: process.env.REACT_APP_FIREBASE_APP_ID

// apiKey: "AIzaSyBgnLUZmWMyhJzal77eI8IaUKAOQ3cc9wo",
// authDomain: "auth-f26dd.firebaseapp.com",
// projectId: "auth-f26dd",
// storageBucket: "auth-f26dd.appspot.com",
// messagingSenderId: "484496990745",
// appId: "1:484496990745:web:9ccbf80493376a11ef0088"

const Firebase = firebase.initializeApp(config);

export const auth = Firebase.auth();

export default Firebase;