import React, { createContext, FC, useState } from 'react';
import { createMuiTheme, createTheme, ThemeProvider } from '@material-ui/core';

interface IThemeContext {
    theme: string;
    toggleTheme?: () => void;
}

const defaultState = {
    theme: 'light'
}

const darkTheme = createTheme({
    palette: {
        type: 'dark',
        text: {
            primary: '#fff',
            secondary: '#516377'
        },
        background: {
            paper: '#111',
            default: '#1f1f1f'
        },
        primary: {
            main: '#1f1f1f'
        }
    }
})

const lightTheme = createTheme({
    palette: {
        type: 'light',
        text: {
            primary: '#20262d',
            secondary: '#f3f6f9'
        },
        background: {
            paper: '#fff',
            default: '#f3f6f9'
        },
        primary: {
            main: '#888fdc'
        }
    }
})

export const ThemeContext = createContext<IThemeContext>(defaultState);

export const ThemeContextProvider: FC = ({ children }) => {
    const [theme, setTheme] = useState<string>(localStorage.getItem("theme") || defaultState.theme);

    const toggleTheme = () => {
        theme === "light" ? setTheme("dark") : setTheme("light")
      };
    
    return (
        <ThemeProvider theme={theme === "dark" ? darkTheme : lightTheme}>
            <ThemeContext.Provider value={{ theme, toggleTheme }}>
                {children}
            </ThemeContext.Provider>
        </ThemeProvider>
    )
}