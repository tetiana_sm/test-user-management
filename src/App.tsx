import React, { FC, useEffect, useState } from 'react';
import  { Redirect } from 'react-router-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';


import SignUp from './pages/signUp/SignUp';
import SignIn from './pages/signIn/SignIn';
import Profile from './pages/profile/Profile';
import ManageUsers from './pages/manage/ManageUsers';
import Header from './components/header/Header';

import { auth } from './config/firebase';
import { getUserById } from './store/actions/auth';

const App: FC = () => {
  const dispatch = useDispatch();
  const [authUser, setAuthUser] = useState<any>();

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        dispatch(getUserById(user.uid));
        setAuthUser(user);
      } else {
        setAuthUser(null)
      }
    })
  }, [])

  return (
    <BrowserRouter>
      <Header />

      <Switch>
        <Route exact path="/" render={() => ( !authUser ? <SignUp /> : <Redirect to='/manageusers' /> )} />
        <Route exact path="/login" render={() => ( !authUser ? <SignIn /> : <Redirect to='/manageusers' /> )} />
        <Route exact path="/manageusers" render={() => ( authUser ? <ManageUsers /> : <Redirect to='/login' /> )} />
        <Route exact path="/profile" component={Profile} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;