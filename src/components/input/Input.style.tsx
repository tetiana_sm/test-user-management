import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
    console.log(theme)
    return ({    
    input: {        
        marginBottom: theme.spacing(2),

        '& .MuiFormLabel-root': {
            color: theme.palette.text.primary
        },

        '& .MuiInputBase-input': {
            padding: theme.spacing(2, 1),
            fontFamily: '"Roboto", sans-serif',
            fontSize: 14,
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.background.default
        },

        '& .MuiOutlinedInput-notchedOutline': {
            borderColor: theme.palette.grey[400]
        },

        '& .MuiOutlinedInput-root': {
            '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
                borderColor: '#888fdc',
                borderWidth: 1
            }
        }
    },
    formMessage: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 14,
        color: '#f00',
        position: 'relative',
        top: theme.spacing(-1.5)
    }
})});

export default useStyles;