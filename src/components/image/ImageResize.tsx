import React, { ChangeEvent, FC } from 'react';
import { Box, TextField } from '@material-ui/core';
import AvatarEditor from 'react-avatar-editor';

interface ImageResizeProps {
    file: any;
    selectFile: Function;
    setEditor: Function;
}

const ImageResize: FC<ImageResizeProps> = ({file, selectFile, setEditor}) => {
    const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.files && event.target.files?.length > 0) {
            selectFile(event.target.files[0])
        }
    }

    function setEditorRef(editor: any) {
        setEditor(editor)
    }

    return (
        <Box display="flex" flexDirection="column">
            <TextField
                type="file"
                onChange={handleFileChange} />

            {
                file && <AvatarEditor
                            image={file}
                            width={100}
                            height={100}
                            borderRadius={100}
                            color={[255, 255, 255, 0.6]}
                            scale={1.2}
                            rotate={0}
                            ref={(ref) => setEditorRef(ref)}
                        /> 
            }
        </Box>
    )
}

export default ImageResize;